<?php

namespace SyliusSf2Bundle\Model;

use Sylius\Component\Resource\Model\ResourceInterface;

/**
 * Class Avenger
 * @package SyliusSf2Bundle\Model
 */
class Avenger implements ResourceInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     * Avenger's public name
     */
    protected $superHeroName;

    /**
     * @var string
     * Avenger's real name
     */
    protected $realName;

    /**
     * @var boolean
     * Is Avenger's identity revealed?
     */
    protected $identityRevealed;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Avenger
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getSuperHeroName()
    {
        return $this->superHeroName;
    }

    /**
     * @param string $superHeroName
     * @return Avenger
     */
    public function setSuperHeroName($superHeroName)
    {
        $this->superHeroName = $superHeroName;
        return $this;
    }

    /**
     * @return string
     */
    public function getRealName()
    {
        return $this->realName;
    }

    /**
     * @param string $realName
     * @return Avenger
     */
    public function setRealName($realName)
    {
        $this->realName = $realName;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIdentityRevealed()
    {
        return $this->identityRevealed;
    }

    /**
     * @param boolean $identityRevealed
     * @return Avenger
     */
    public function setIdentityRevealed($identityRevealed)
    {
        $this->identityRevealed = $identityRevealed;
        return $this;
    }
}