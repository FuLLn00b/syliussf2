<?php

namespace SyliusSf2Bundle\Form\Type;

use Sylius\Bundle\ResourceBundle\Form\Type\AbstractResourceType;
use Symfony\Component\Form\FormBuilderInterface;

class AvengerType extends AbstractResourceType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('superHeroName', 'text', array('label' => "Nom du super héros", 'attr' => array('placeholder' => "Nom du super héros", 'class' => 'form-control')))
            ->add('realName', 'text', array('label' => "Identité réelle", 'attr' => array('placeholder' => "Identité réelle", 'class' => 'form-control')))
            ->add('identityRevealed', 'checkbox', array('label' => 'Identité révélé?', 'required' => false));
    }

    public function getName()
    {
        return 'sylius_sf2_avenger';
    }
}